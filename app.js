const {ApolloServer, gql} = require('apollo-server');
const {typeDefs} = require('./schema')
const {resolvers} = require('./resolvers')
const mongoose = require('mongoose')

const server = new ApolloServer ({
    typeDefs,
    resolvers
})

mongoose.connect('mongodb+srv://admin:dev28@b106.nrlsy.mongodb.net/QuizzApp?retryWrites=true&w=majority');
mongoose.connection.once('open', () => {
    console.log("Connected to database..")
})


server.listen().then(({url}) => {
    console.log('Server is running at port: ' + url);
})