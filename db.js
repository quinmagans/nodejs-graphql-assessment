const questions = [
    {
        id: "61f3da10b1d4cf2b2c22fe9c",
        title: "What is the capital of the Philippines?"
    },
    {
        id: "61f3e213a4ce6bf7621bc48d",
        title: "3/4 is an example of?"
    },
    {
        id: "61f3e236a4ce6bf7621bc48f",
        title: "What is an Asia?"
    },
    {
        id: "61f3e24aa4ce6bf7621bc491",
        title: "The planet closest to the Sun is?"
    },
    {
        id: "61f3e27aa4ce6bf7621bc493",
        title: "How many continents are there?"
    }
];

const answers = [
    {
        id: "61f3ded90a82445c63509cdb",
        text: ["a. Manila", "b. Davao", "c. Cebu", "d. Siargao"],
        isCorrect: 0,
        questionId: "61f3da10b1d4cf2b2c22fe9c"
    },
    {
        id: "61f3e2f0a4ce6bf7621bc495",
        text: ["a. A decimal", "b. A fraction", "c. An odd number", "d. A rational number"],
        isCorrect: 1,
        questionId: "61f3e213a4ce6bf7621bc48d"
    },
    {
        id: "61f3e353a4ce6bf7621bc49b",
        text: ["a. A country", "b. A state", "c. An island", "d. A continent"],
        isCorrect: 3,
        questionId: "61f3e236a4ce6bf7621bc48f"
    },
    {
        id: "61f3e386a4ce6bf7621bc49d",
        text: ["a. Earth", "b. Mercury", "c. Jupiter", "d. Mars"],
        isCorrect: 1,
        questionId: "61f3e24aa4ce6bf7621bc491"
    },
    {
        id: "61f3e3b3a4ce6bf7621bc49f",
        text: ["a. Five", "b. Seven", "c. Six", "d. Eight"],
        isCorrect: 1,
        questionId: "61f3e27aa4ce6bf7621bc493"
    }
];

const users = [
    {
        email: "test@mail.com"
    },
    {
        email: "example@mail.com"
    }
];

// const correct = [
//     {
//         isCorrect: true,
//         questionId: ""
//     }
// ]


module.exports = {
    questions,
    answers,
    users
}