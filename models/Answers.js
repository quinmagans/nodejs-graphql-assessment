const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;

const answersSchema = new Schema({
    text: [String],
    isCorrect: true,
    questionId: String
})

module.exports = mongoose.model('Answers', answersSchema)