const { answers, questions, users } = require("./db");

exports.resolvers = {
    Query: {
        questions () {
            return questions
        },
        answers (parent, {filter}, {products}) {
            let correctAnswer = answers;

            if(filter) {
                if(filter.isCorrect === true) {
                    correctAnswer = correctAnswer.filter((answer) => {
                        return answer.isCorrect;
                    });
                }
            }
            return correctAnswer;
        },
        users () {
            return users
        }
    },
    Question: {
        answers(parent) {
            return answers.filter((answer) => answer.questionId === parent.id);
        },
    },
    Answer: {
        question(parent) {
            return questions.find((question) => question.id === parent.questionId);
        }
    }
}

