const {ApolloServer, gql} = require('apollo-server');

exports.typeDefs = gql`

    type Question {
        id: ID!
        title: String
        questionId: String
        answers(filter: CorrectAnswerInput): [Answer]
        
    }

    type Answer {
        id: ID!
        text: [String]
        questionId: String!
        isCorrect: Int
        question: Question
    }

    type User {
        email: String
    }

    input CorrectAnswerInput {
        isCorrect: Boolean
    }

    type Query {
        questions: [Question]
        answers: [Answer]
        users: [User]
    }

`